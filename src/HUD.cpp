#include "HUD.h"
#include "StateManager.h"
#include "ResourceManager/ResourceIdentifier.h"
#include "ResourceManager/ResourceHolder.h"

using namespace gui;

HUD::HUD(States::Context context, World::GameData &gameData)
    : context(context)
    , gameData(&gameData)
{
    background.setFillColor(sf::Color(169, 169, 169));
    background.setPosition(0.f, 480.f);
    background.setSize({960, 120});

    auto audio = std::make_shared<gui::Icon>();
    audio->setTexture(context.textureHolder->get(Textures::audioOn), context.textureHolder->get(Textures::audioOff));
    audio->setPosition({770.f, 10.f});
    audio->setCallback([this]()
    {
        action = Action::Audio;
    });



    auto music = std::make_shared<gui::Icon>();

    music->setTexture(context.textureHolder->get(Textures::musicOn), context.textureHolder->get(Textures::musicOff));
    music->setPosition({820.f, 10.f});
    music->setCallback([this]
    {
       action = Action::Music;
    });

    auto pause = std::make_shared<gui::Icon>();

    pause->setTexture(context.textureHolder->get(Textures::pauseOff), context.textureHolder->get(Textures::pauseOff));
    pause->setPosition({670.f, 10.f});
    pause->setCallback([this]
                       {
                           action = Action::Pause;


                       });

    auto forward = std::make_shared<gui::Icon>();

    forward->setTexture(context.textureHolder->get(Textures::forwardOff), context.textureHolder->get(Textures::forwardOn));
    forward->setPosition({600.f, 10.f});
    forward->setCallback([this]
                       {
                           action = Action::Forward;
                       });


    auto exit = std::make_shared<gui::Icon>();

    exit->setTexture(context.textureHolder->get(Textures::exit), context.textureHolder->get(Textures::exit));
    exit->setPosition({890.f, 10.f});
    exit->setCallback([this]
                             {
                                 action = Action::Exit;

                             });

    auto upgrade = std::make_shared<gui::Button>(*context.soundManager);
    upgrade->setTexture(context.textureHolder->get(Textures::button));
    upgrade->setPosition({600.f, 485.f});
    upgrade->setFont(*context.font);
    upgrade->setText("Upgrade");
    upgrade->setCallback([this]()
    {
        action = Action::Upgrade;
    });

    auto sell = std::make_shared<gui::Button>(*context.soundManager);
    sell->setTexture(context.textureHolder->get(Textures::button));
    sell->setPosition({ 600.f, 550.f});
    sell->setFont(*context.font);
    sell->setText("Sell");
    sell->setCallback([this]()
    {

        action = Action::Sell;
    });

    container.addWidget(audio);
    container.addWidget(music);
    container.addWidget(pause);
    container.addWidget(forward);
    container.addWidget(exit);

    //container.addWidget(upgrade);
    //container.addWidget(sell);

    action = Action::None;
}

void HUD::init()
{

    gold.setTexture(context.textureHolder->get(Textures::gold));
    gold.setPosition(0.f, 0.f);

    star.setTexture(context.textureHolder->get(Textures::star));
    star.setPosition(250.f, 5.f);

    live.setTexture(context.textureHolder->get(Textures::lives));
    live.setScale({1.f / 3.f, 1.f / 3.f});
    live.setPosition(150.f, 5.f);


    fight.setTexture(context.textureHolder->get(Textures::fight));
    fight.setPosition(430.f, 5.f);

    //singleTurret.setTexture(context.textureHolder->get(Textures::towerOneBase));
    //singleTurret.setPosition(268.f, 500.f);

    //singleTurret.setTexture(context.textureHolder->get(Textures::towerOneBase));
    //splashTurret.setPosition(368.f, 500.f);

    totalGold.setFont(*context.font);
    totalGold.setString(std::to_string(gameData->gold));
    totalGold.setPosition(50.f, 10.f);

    totalLives.setFont(*context.font);
    totalLives.setString(std::to_string(gameData->lives));
    totalLives.setPosition(200.f, 10.f);

    currentLevel.setFont(*context.font);
    currentLevel.setString(std::to_string(gameData->currentLevel));
    currentLevel.setPosition(450.f, 50.f);

    score.setFont(*context.font);
    score.setString(std::to_string(gameData->score));
    score.setPosition(300.f, 10.f);
}

void HUD::handleEvent(const sf::Event &event)
{
    sf::Vector2i mousePos = sf::Mouse::getPosition(*context.window);
    if (event.type == sf::Event::MouseButtonPressed
        && event.key.code == sf::Mouse::Left)
    {
    }
    else if (event.type == sf::Event::MouseButtonPressed
            && event.key.code == sf::Mouse::Right)
    {
        action = Action::None;
    }

    container.handleWidgetsEvent(event);
}

void HUD::update(sf::Time dt)
{
    totalGold.setString(std::to_string(gameData->gold));
    totalLives.setString(std::to_string(gameData->lives));
    currentLevel.setString(std::to_string(gameData->currentLevel + 1));
    score.setString(std::to_string(gameData->score));

    container.updateWidgets(dt);
}




void gui::HUD::setAction(Action action)
{
    this->action = action;
}

const HUD::Action HUD::getAction() const
{
    return action;
}

void HUD::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    states.transform *= getTransform();

    target.draw(gold);
    target.draw(live);
    target.draw(fight);
    target.draw(star);
    target.draw(totalGold);
    target.draw(totalLives);
    target.draw(currentLevel);
    target.draw(score);
    target.draw(container);
}