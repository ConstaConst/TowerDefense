//
// Created by Falcon on 29.04.17.
//

#pragma once

#include "State.h"
#include "Gui.h"
#include "ResourceManager/ResourceHolder.h"
#include "ResourceManager/ResourceIdentifier.h"
#include "Grid.h"
#include "TileMap.h"
#include "Camera.h"
#include "Tower.h"
#include "Enemy.h"
#include "Bullet.h"
#include <SFML/System/Time.hpp>
#include "HUD.h"

class GameState : public State
{
public:
    explicit GameState(StateManager &stack, States::Context context);
    ~GameState() = default;

    bool handleEvent(const sf::Event &event) override;
    bool update(sf::Time dt) override ;
    void draw() override;

    void createWave();

private:

    World::GameData gameData;

    bool fastForward;

    sf::Time waitingTime;

    sf::RectangleShape placement;
    sf::CircleShape range;

    Camera camera;

    gui::Gui container;

    std::list<Enemy> enemies;

    std::vector<Tower> towers;
    std::list<Bullet> bullets;

    std::vector<sf::Vector2f> dest;
    size_t waveEnemy;
    float distanceEnemy;
    size_t numHardEnemy;

    Grid grid;
    gui::HUD hud;
    gui::HUD::Action action;
    sf::Texture texture;
    TileMap map;

    sf::Music music;

    bool soundPlay;
    bool musicPlay;

    sf::Text countdown;

};



