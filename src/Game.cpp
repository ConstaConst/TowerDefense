//
// Created by Falcon on 29.04.17.
//

#include "Game.h"
#include "MenuState.h"
#include "GameState.h"
#include "PauseState.h"
#include "GameOverState.h"
#include "WinState.h"

#include "ResourceManager/ResourceIdentifier.h"
#include "ResourceManager/ResourceHolder.h"

Game::Game()
    : window({960, 768}, "TD", sf::Style::Titlebar | sf::Style::Close, sf::ContextSettings{0, 0, 8, 1, 1, 0, false})
    , context(window, font, textureHolder, fontHolder, soundManager, cursor, fastForward)
    , manager(context)
    , fastForward(fastForward)
{
    fastForward.setFastForward(false);
    window.setMouseCursorVisible(false);

    fontHolder.load(Fonts::font1, "Resources/sansation.ttf");
    textureHolder.load(Textures::cursor, "Resources/cursor.png");
    textureHolder.load(Textures::panel, "Resources/background2.jpg");
    textureHolder.load(Textures::button, "Resources/button.png");

    textureHolder.load(Textures::audioOn, "Resources/audioOn.png");
    textureHolder.load(Textures::audioOff, "Resources/audioOff.png");

    textureHolder.load(Textures::musicOn, "Resources/musicOn.png");
    textureHolder.load(Textures::musicOff, "Resources/musicOff.png");

    textureHolder.load(Textures::pauseOn, "Resources/pauseOn.png");
    textureHolder.load(Textures::pauseOff, "Resources/pauseOff.png");

    textureHolder.load(Textures::forwardOn, "Resources/fastForwardOn.png");
    textureHolder.load(Textures::forwardOff, "Resources/fastForwardOff.png");

    textureHolder.load(Textures::fastForwardOn, "Resources/forwardOn.png");
    textureHolder.load(Textures::fastForwardOff, "Resources/forwardOff.png");

    textureHolder.load(Textures::fight, "Resources/fight.png");
    textureHolder.load(Textures::exit, "Resources/exit.png");
    textureHolder.load(Textures::star, "Resources/star.png");


    soundManager.loadFromFile("mouseHover", "Resources/click4.wav");
    soundManager.loadFromFile("mouseClick", "Resources/click4.wav");
    cursor.setTexture(textureHolder.get(Textures::cursor));
    font = fontHolder.get(Fonts::font1);

    registerStates();
    manager.pushState(States::ID::Menu);
}

void Game::run()
{
    const sf::Time frameTime = sf::seconds(1.f / 30.f);
    sf::Clock clock;
    sf::Time passedTime = sf::Time::Zero;

    while (window.isOpen())
    {
        sf::Time elapsedTime = clock.restart();

        if (fastForward.getFastForward())
            passedTime += elapsedTime + elapsedTime + elapsedTime + elapsedTime;
        else
            passedTime += elapsedTime;

        while (passedTime > frameTime)
        {
            passedTime -= frameTime;

            processEvents();
            update(frameTime);

            if (manager.isEmpty())
                window.close();
        }

        draw();
    }
}

void Game::processEvents()
{
    sf::Event event;
    while (window.pollEvent(event))
    {
        if (event.type == sf::Event::Closed)
            window.close();

        manager.processEvents(event);
    }
}

void Game::update(sf::Time frameTime)
{
    manager.update(frameTime);

    cursor.setPosition(static_cast<sf::Vector2f>(sf::Mouse::getPosition(window)));
}

void Game::draw()
{
    window.clear(sf::Color(210, 210, 210));

    manager.draw();
    window.draw(cursor);

    window.display();
}

void Game::registerStates()
{
    manager.registerState<GameState>(States::ID::Game);
    manager.registerState<MenuState>(States::ID::Menu);
    manager.registerState<PauseState>(States::ID::Pause);
    manager.registerState<GameOverState>(States::ID::GameOver);
    manager.registerState<WinState>(States::ID::Win);
}

