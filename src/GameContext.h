#pragma once

#include "ResourceManager/ResourceIdentifier.h"
#include "ResourceManager/ResourceHolder.h"
#include "FastForward.h"

enum class Type
{
    lvlZero,
    lvlOne,
    lvlTwo,
};

namespace sf
{
    class RenderWindow;

    class Font;

    class Sprite;
}

class SoundManager;

namespace World
{
    struct GameData
    {
        int lives;
        int gold;
        int currentLevel;
        int score;
    };
}

namespace States
{
    struct Context
    {
        explicit Context(sf::RenderWindow &window
        , sf::Font &font
        , TextureHolder &textureHolder
        , FontHolder &fontHolder
        , SoundManager &soundManager
        , sf::Sprite &cursor
        , FastForward &fastForward)

        : window(&window)
        , font(&font)
        , textureHolder(&textureHolder)
        , fontHolder(&fontHolder)
        , soundManager(&soundManager)
        , cursor(&cursor)
        , fastForward(&fastForward)

        {};

        sf::RenderWindow *window;
        sf::Font *font;
        TextureHolder *textureHolder;
        FontHolder *fontHolder;
        SoundManager *soundManager;
        sf::Sprite *cursor;
        FastForward *fastForward;
    };
}