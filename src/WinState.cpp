#include "WinState.h"

#include <fstream>
#include <cmath>

WinState::WinState(StateManager &stack, States::Context context)
        : State(stack, context)
{
    sf::RenderWindow &window = *context.window;
    sf::Font &font = *context.font;

    winText.setString("YOU WIN");
    winText.setFillColor(sf::Color::Green);
    winText.setCharacterSize(100);
    winText.setFont(font);
    winText.setPosition({ std::floor(window.getSize().x / 2 - winText.getLocalBounds().width / 2),
                               std::floor(window.getSize().y / 2 - winText.getLocalBounds().height * 1.5f)});

    enterText.setString("ENTER Menu");
    enterText.setFillColor(sf::Color::Black);
    enterText.setCharacterSize(30);
    enterText.setFont(font);
    enterText.setPosition({ std::floor(window.getSize().x / 2 - enterText.getLocalBounds().width / 2),
                            std::floor(window.getSize().y / 2 + enterText.getLocalBounds().height * 5)});
}

bool WinState::handleEvent(const sf::Event &event)
{
    if (event.type == sf::Event::KeyPressed
        && event.key.code == sf::Keyboard::Return)
    {
        popState();
        pushState(States::ID::Menu);
    }

    return false;
}

bool WinState::update(sf::Time dt)
{
    return false;
}

void WinState::draw()
{
    sf::RenderWindow &window = *getContext().window;
    window.setView(window.getDefaultView());
    window.draw(winText);
    window.draw(enterText);
}