#pragma once

class FastForward {

public:
    FastForward();

    bool getFastForward() const;

    void setFastForward(bool key);

private:
    bool key;
};


