#pragma once

#include "ResourceManager/ResourceHolder.h"
#include "ResourceManager/ResourceIdentifier.h"
#include "Bullet.h"
#include "Enemy.h"
#include "GameContext.h"
#include <SFML/Graphics.hpp>
#include <cstddef>
#include <SFML/Graphics/CircleShape.hpp>
#include <memory>
#include <list>
#include "GameContext.h"
#include "Constants.h"
#include "GameContext.h"

class Bullet;

class Tower {
public:
    Tower (TextureHolder &textures, const sf::Vector2f &position, States::Context context, World::GameData &gameData);
    void updateTower (std::list<Enemy>& enemies, sf::Time& dTime, std::list<Bullet> &bullets);
    void draw(sf::RenderWindow &window);
    void handleEventTower(const sf::Event &event);
    void shoot(sf::Time &dTime, std::list<Bullet> &bullets);

    sf::Vector2f getPosition() const;
    float getAngle() const;

private:
    Type type;
    Type bulletType;
    size_t attackRange;
    size_t attackSpeed;
    double attackCooldown;
    size_t price;
    World::GameData *gameData;
    bool priceVisible;
    bool rangeVisible;


    void upgradeTower();


    Enemy* targetEnemy;



    States::Context context;
    sf::RenderWindow *window;
    sf::Sprite topSprite;
    sf::Sprite baseSprite;
    sf::CircleShape circleShape;
    sf::Text priceText;
    int width, height;
    float angle;
    TextureHolder& texture;
    //Bullet bullet;

    bool inRange (const sf::Vector2f& position) const;
    float calculatingAngle (const sf::Vector2f& position);
};


